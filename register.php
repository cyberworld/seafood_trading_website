 
 <?php
 session_start();
//定义个常量，用来授权调用includes里面的文件
define('IN_TG',true);
//定义个常量，用来指定本页的内容
define('SCRIPT','register');
//引用公共文件
require dirname('_FILE_').'/includes/common.inc.php';

//判断是否提交了

if(@$_GET['action']=='register'){
	//为了防止恶意注册，跨站攻击
	if(!($_POST['yzm']==$_SESSION['code'])){
		_alert_back('验证码不正确！');
	}
	//创建一个空数组，用来存放提交过来的合法数据
	$_clean=array();
	$_clean['username']=$_POST['username'];
	$_clean['password']=$_POST['password'];
	print_r($_clean);
}

?>



<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Cyber World--注册</title>

<?php 
	require ROOT_PATH.'includes/title.inc.php';
?>

<script type="text/javascript" src="js/face.js"></script>

</head>
<body>

<?php 
	require ROOT_PATH.'includes/header.inc.php';
?>


<div id="register">
<h2>会员注册</h2>
<form method="post" name="register" action="register.php?action=register">

	<dl>
		<dt>请认真填写</dt>
		<dd>用 户 名:<input type="text" name="username" class="text"> (*必填，至少两位)</dd>
		<dd>密 &nbsp;&nbsp;码 :<input type="password" name="password" class="text"> (*必填，至少六位)</dd>
		<dd>确认密码:<input type="password" name="notpassword" class="text"> (*必填，同上)</dd>
		<dd> 性 别  :<input type="radio" name="sex" value="男" checked="checked" >男&nbsp;&nbsp;&nbsp;<input type="radio" name="sex" value="女">女</dd>
		<dd class="face"><input type="hidden" name="face" value="face/1.jpg"><img src="face/1.jpg" width="150px" height="100px" alt="头像选择" id="faceimg" ></dd>
		<dd>电子邮件:<input type="text" name="email" class="text"></dd>
		<dd> Q Q  &nbsp;&nbsp;&nbsp; :<input type="text" name="qq" class="text"></dd>
		<dd>主页地址:<input type="text" name="url" class="text" value="http://"></dd>
		<dd>验 证 码 : <input type="text" name="yzm" class="text yzm"><img src="code.php" id="code" ></dd>
		<dd>     <input type="submit" class="submit" value="注册"></dd>
	</dl>
</form>
</div>
</body>
</html>